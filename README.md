﻿# nw_aggr_map.py

Produces an aggregation map of a Netwitness environment in a text-based data file, for use with automated graph-generation tools (such as [GraphViz](https://en.wikipedia.org/wiki/Graphviz)).  With this tool, you can produce a usable environment map in just minutes!

## Getting Started

### Supported Versions

Initially tested on Netwitness 11.1.0.0 (CentOS 7)

### Prerequisites

This script requires the **all-systems** file produced by John Snider's [11.X version of the get-all-systems.sh](mailto:john.snider@rsa.com) script.  The default location for this **all-systems** file, once produced, is `/var/netwitness/nw-backup/all-systems`

The script will only show linkages between nodes where there is active aggregation.  If the aggregation links have not been set up yet or aggregation has been disabled for a while, the linkage may not show up in the map.  So, make sure aggregation is already configured and running.

Once the output file is produced, you would need to install whatever tools you need to render the graphic from the data.  See the [Use Case Examples](#Use-Case-Examples) for more info.

### Install

This script uses standard Python libraries included in the default Netwitness install.  Just copy the script into your Admin server (Node0), make it executable and run it.  You can obtain the script from its [GitLab](https://gitlab.com/mitchrsa/nw_aggr_map)

## Basic Usage

### Run the script to get the mapping data

This will display the output to screen.

``` sh
nw_aggr_map.py
```

To dump to a file just pipe the output

``` sh
nw_aggr_map.py > sample_nw_map.gv
```

### Produce a graph from the mapping data

See the [Use Case Examples](#Use-Case-Examples) below for examples on how to generate graphs from this data.

>As of this writing, the script only outputs the data into the GraphViz [DOT language](https://en.wikipedia.org/wiki/DOT_(graph_description_language)).

## Use Case Examples

### Using graphviz and pydot

The GraphViz toolset is open source and there is a good Python command-line interface available called [PyDot](https://pypi.org/project/pydot/), which is shown below.  If you have trouble getting it installed via pip, you can try your official Linux repo as described in [this post](https://askubuntu.com/questions/917030/how-to-install-pydot-and-graphviz).

Once you have PyDot installed, simply run it against your data file to produce an image file (in this case a png).

``` sh
dot -Tpng -o sample_nw_map.png sample_nw_map.gv
```

This will produce a graphic similar to this:

![sample netwitness environment](sample_nw_map.png "Sample Netwitness Environment")

There are [other tools](https://en.wikipedia.org/wiki/DOT_(graph_description_language)#Layout_programs) which can render graphs in the DOT language, such as:

* Viz.js - for quick online rendering
* Gephi
* and several others...

Here are a couple of good posts to show the types of graphs you can generate with GraphViz:

* [https://spin.atomicobject.com/2013/01/30/making-diagrams-with-graphviz/](https://spin.atomicobject.com/2013/01/30/making-diagrams-with-graphviz/)
* [https://renenyffenegger.ch/notes/tools/Graphviz/examples/index](https://renenyffenegger.ch/notes/tools/Graphviz/examples/index)

### About Visio

My current understanding is that Visio can auto-generate objects and links into a diagram via the Excel Data Visualizer tool.  My past experience with auto-generating Visio diagrams was that it was not entirely "auto".  The objects would initially be all jumbled and you have to arrange them the way you want manually.  For me, this defeated the benefit of producing the diagram in an automated fashion.

I would certainly be open to adding an output format to this script that would work for Visio if there is enough interest.

## Modifying the Graph Properties

If you want to modify how the graph appears when it is rendered, you can either edit the script to change the defaults or you can modify the DOT file after the script is run but before you render the file to a graph.

Check the [GraphViz/DOT documentation](https://www.graphviz.org/doc/info/) for info on options you can use for objects and connectors.

### Modifying the script defaults

There is a Python dictionary defined at the top of the script, which sets properties for each type of node found in the all-systems file.  You can feel free to edit the defaults here.

``` python
def get_svc_info(node_type):
    "LogCollector": {
      "svc_type": "LogCollector",
      "parent_port": 56001, 
      "parent_cmd": "event-broker/destinations list force-content-type=text/xml", 
      "child_port": 0,
      "child_cmd": "", 
      "fillcolor": "gold", 
      "fontcolor": "black",
      "shape": "box"
    },
    "LogDecoder": {
      "svc_type": "LogDecoder",
      "parent_port": 56002, 
      "parent_cmd": "decoder whoAgg", 
      "child_port": 56001,
      "child_cmd": "event-broker/sources list", 
      "fillcolor": "dodgerblue", 
      "fontcolor": "white",
      "shape": "box"
    },
    "Broker": {
      ...
      ...
```

### Modifying the DOT file by hand before rendering

Since this script will produce a data file before you render it, you can simply make manual edits to this file to manipulate the rendering.  This includes adding or removing nodes, adding text to the arrows or changing the appearance of objects.

The **digraph** object within the DOT file contains a top section, which defines each object and its properties.  It looks like this:

``` gv
    mynode0 [shape=box, label=<mynode0<br/>10.0.0.10>, fontcolor=white, style=filled, fillcolor=purple]
    mybroker1 [shape=box, label=<mybroker1<br/>10.0.0.11>, fontcolor=white, style=filled, fillcolor=purple]
```

You can add/remove nodes or just modify the properties of objects here.  As for the linkages, there is another section at the bottom of the DOT file which looks like this:

``` gv
    mynode0 -> mybroker1 [style=solid]
    mynode0 -> mybroker2 [style=solid]
    mybroker1 -> myconcentrator1 [style=solid]
```

## To Do

* Add error trapping!!
* Add optional argument for all-systems file path
* Move the code and the service dictionary definition to class files to add modularity and facilitate additional output formats
* Consider adding output format for importing to Visio
* Make this work with 10.6.6 as well

<!-- ## Contributing

Please read [CONTRIBUTING.md](https://) for details on our code of conduct, and the process for submitting pull requests to us. -->

## Authors

* **Mitch Hanks** - *Initial work* - [GitLab repo](https://gitlab.com/mitchrsa/nw_aggr_map)

<!-- See also the list of [contributors](https://gitlab.com/your/project/contributors) who participated in this project. -->

<!-- ## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details -->

## Acknowledgments

* John Snider
* Naushad Kasu

## References

* https://en.wikipedia.org/wiki/Graphviz
* https://spin.atomicobject.com/2013/01/30/making-diagrams-with-graphviz/
* https://pythonhaven.wordpress.com/2009/12/09/generating_graphs_with_pydot/
* https://renenyffenegger.ch/notes/tools/Graphviz/examples/index
* https://www.graphviz.org/doc/info/
* http://www.graphviz.org/pdf/dotguide.pdf
* https://github.com/mdaines/viz.js
