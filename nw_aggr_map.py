#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
nw_aggr_map.py
Get Netwitness aggregation map data and output it to a usable graph data file (i.e. DOT format for use with GraphViz and other tools).
@author: Mitch Hanks (mitchell.hanks@rsa.com)
@url: 
"""

import sys
import re
import subprocess
import shlex
import csv

def get_svc_info(node_type):
	# Create a service dictionary and assign pertinent info
	# This will define what port and API command to use.
	# See the following for more info on DOT syntax:
	# https://www.graphviz.org/doc/info/
	# TODO: Convert this to a class along with the output function
	svc_info_map = {
		"LogCollector": {
			"svc_type": "LogCollector",
			"parent_port": 56001, 
			"parent_cmd": "event-broker/destinations list force-content-type=text/xml", 
			"child_port": 0,
			"child_cmd": "", 
			"fillcolor": "gold", 
			"fontcolor": "black",
			"shape": "box"
		},
		"LogDecoder": {
			"svc_type": "LogDecoder",
			"parent_port": 56002, 
			"parent_cmd": "decoder whoAgg", 
			"child_port": 56001,
			"child_cmd": "event-broker/sources list", 
			"fillcolor": "dodgerblue", 
			"fontcolor": "white",
			"shape": "box"
		},
		"Broker": {
			"svc_type": "Broker", 
			"parent_port": 56003, 
			"parent_cmd": "broker whoAgg", 
			"child_port": 0,
			"child_cmd": "", 
			"fillcolor": "purple", 
			"fontcolor": "white",
			"shape": "box"
		},
		"AdminServer": {
			"svc_type": "Broker", 
			"parent_port": 56003, 
			"parent_cmd": "broker whoAgg", 
			"child_port": 0,
			"child_cmd": "", 
			"fillcolor": "purple", 
			"fontcolor": "white",
			"shape": "box"
		},
		"Decoder": {
			"svc_type": "NetworkDecoder", 
			"parent_port": 56004, 
			"parent_cmd": "decoder whoAgg", 
			"child_port": 0,
			"child_cmd": "", 
			"fillcolor": "dodgerblue", 
			"fontcolor": "white",
			"shape": "box"
		},
		"Concentrator": {
			"svc_type": "Concentrator", 
			"parent_port": 56005, 
			"parent_cmd": "concentrator whoAgg", 
			"child_port": 0,
			"child_cmd": "", 
			"fillcolor": "forestgreen", 
			"fontcolor": "white",
			"shape": "box"
		},
		"NetworkHybrid": {
			"svc_type": "Concentrator", 
			"parent_port": 56005, 
			"parent_cmd": "concentrator whoAgg", 
			"child_port": 0,
			"child_cmd": "", 
			"fillcolor": "forestgreen", 
			"fontcolor": "white",
			"shape": "box"
		},
		"LogHybrid": {
			"svc_type": "Concentrator", 
			"parent_port": 56005, 
			"parent_cmd": "concentrator whoAgg", 
			"child_port": 0,
			"child_cmd": "", 
			"fillcolor": "forestgreen", 
			"fontcolor": "white",
			"shape": "box"
		},
		"Archiver": {
			"svc_type": "Archiver", 
			"parent_port": 56008, 
			"parent_cmd": "archiver whoAgg", 
			"child_port": 0,
			"child_cmd": "", 
			"fillcolor": "darkorange", 
			"fontcolor": "black",
			"shape": "box"
		},
		"ESAPrimary": {
			"svc_type": "ESA", 
			"parent_port": 0, 
			"parent_cmd": "", 
			"child_port": 0,
			"child_cmd": "", 
			"fillcolor": "red", 
			"fontcolor": "white",
			"shape": "box"
		},
		"ESASecondary": {
			"svc_type": "ESA", 
			"parent_port": 0, 
			"parent_cmd": "", 
			"child_port": 0,
			"child_cmd": "", 
			"fillcolor": "red", 
			"fontcolor": "white",
			"shape": "box"
		}
	}
	return svc_info_map.get(node_type)

def get_host_list(all_systems_file):
	with open(all_systems_file, 'rb') as f:
		reader = csv.reader(f)
		return list(reader)

def get_svc_list(hosts):
	services = []
	for host in hosts:
		# Merge all-systems info with our service dictionary
		svc_info = get_svc_info(host[0])
		if svc_info is not None:
			svc_info['ip'] = host[2]
			svc_info['hostname'] = host[1]
			services.append(svc_info)
	return services

def run_nwconsole(address,port,command):
	cmd = "NwConsole -c tlogin server=" + address + " port=" + port + " username=admin group=Administrators cert=/etc/pki/nw/node/node.pem -c " + command
	args = shlex.split(cmd)
	p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out, err = p.communicate()
	return out


def get_aggr_map(services):
	aggr_map = []
	pattern = re.compile('(?:ip=|address=)([0-9\.]+)')
	for child_svc in services:
		if (child_svc['parent_port'] <> 0 and child_svc['parent_cmd'] <> ""):
			# Get raw output from API call
			out = run_nwconsole(child_svc['ip'],str(child_svc['parent_port']),child_svc['parent_cmd'])
			
			# Parse out all of the IP addresses
			match_list = set(pattern.findall(out))
			for match in match_list:
				for parent_svc in services:
					# find parent service by IP
					# If IP's were not unique and we were doing service-based then we
					# would need to figure out another way to map here			
					if match == parent_svc['ip']:
						mapping = [parent_svc['hostname'],child_svc['hostname']] 
						aggr_map.append(mapping)
		if (child_svc['child_port'] <> 0 and child_svc['child_cmd'] <> ""):
			# Get raw output from API call
			out = run_nwconsole(child_svc['ip'],str(child_svc['child_port']),child_svc['child_cmd'])
			
			# Parse out all of the IP addresses
			match_list = set(pattern.findall(out))
			for match in match_list:
				for parent_svc in services:
					# find parent service by IP
					# If IP's were not unique and we were doing service-based then we
					# would need to figure out another way to map here			
					if match == parent_svc['ip']:
						mapping = [child_svc['hostname'],parent_svc['hostname']] 
						aggr_map.append(mapping)
	return aggr_map	

def print_dot(services,aggr_map):
	print('digraph{')
	for service in services:
		print('    ' + service['hostname'] + ' [shape=' + service['shape'] + ', label=<' + service['hostname'] + '<br/>' + service['ip'] + '>, fontcolor=' + service['fontcolor'] + ', style=filled, fillcolor=' + service['fillcolor'] + ']')
	for mapping in aggr_map:
		print('    ' + mapping[0] + ' -> ' + mapping[1] + ' [style=solid]')
	print('}')
	
def main():
	# TODO: Make this an optional argument
	hosts = get_host_list('/var/netwitness/nw-backup/all-systems')
	services = get_svc_list(hosts)
	aggr_map = get_aggr_map(services)
	# TODO: Modularize the output formatting (and the "svc_info" definition)
	print_dot(services,aggr_map)

if __name__ == '__main__':
	main()